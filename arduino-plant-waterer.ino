int humidity = A0;
int greenLed = 2;
int yellowLed = 3;
int redLed = 4;
int hour = 1000*3600;

void setup() {
  Serial.begin(9600);
  pinMode(A0, INPUT);
	pinMode(greenLed, OUTPUT);
	pinMode(yellowLed, OUTPUT);
	pinMode(redLed, OUTPUT);
}

void loop() {
  delay(1000);

  int humidity = analogRead(A0);
	
	if (humidity < 300) {
		digitalWrite(greenLed, HIGH);
	} else if (humidity < 500) {
		digitalWrite(yellowLed, HIGH);
	} else {
		digitalWrite(redLed, HIGH);
	}

  Serial.print("humidity: ");
  Serial.println(humidity);
}